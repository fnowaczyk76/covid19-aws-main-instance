# Las respuestas de boto3 son JSON, los objetos tienen un campo Tags donde se guardan todos los key-values que 
# uno setea en AWS-Console, entre esos, el más clásico es el campo Name.
# Por eso defino la siguiente función, para contener la lógica de consultar el value de cierta key en el conjunto tags de algún objeto
# Si no se le setea ningun nombre, el campo Tags puede no existir.
def getValue(key_name, json_object):
    try:
        result = list(filter(lambda tag: tag['Key'] == key_name, json_object['Tags']))[0]['Value']
    except:
        result = 'N/A'
    return result

# Por lo general, de los objetos quiero ver su Name precisamente y su Id
def printObject(json_object, id_prefix):
    print(f"(name: {getValue('Name', json_object)}, id: {json_object[id_prefix+'Id']})")
    
# Defino una función para construir los comandos a ejecutar en la instancia principal y poder montar el EFS
def build_efs_command_for(instance, efs_volume):
    current_instance_id = instance['InstanceId']
    efs_id = efs_volume['FileSystemId']
    output_file = f'probando-{current_instance_id}.txt'
    message = f'"hola, esto es una prueba para escribir en el volumen compartido {efs_id}, desde la instancia {current_instance_id}."'
    return f"\
        cd / &&\
        touch empezando-en-{current_instance_id}.txt &&\
        \
        (sudo mkdir /tmp/efs-utils || True) &&\
        cd /tmp/efs-utils &&\
        sudo apt update &&\
        sudo apt upgrade &&\
        \
        ((sudo apt install -y binutils &&\
        git clone https://github.com/aws/efs-utils /tmp/efs-utils &&\
        ./build-deb.sh &&\
        sudo apt-get -y install ./build/amazon-efs-utils*deb) || True) &&\
        \
        ((sudo apt install -y nfs-common &&\
        sudo mkdir /home/ubuntu/efs) || True) &&\
        sudo mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport {efs_id}.efs.{region_name}.amazonaws.com:/ /home/ubuntu/efs &&\
        cd /home/ubuntu/efs &&\
        sudo chmod go+rw . &&\
        touch test-file-{current_instance_id}.txt &&\
        echo {message} > {output_file}"