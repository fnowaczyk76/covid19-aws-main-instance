from src.cloud.aws_abstract_simulator import AWSAbstractSimulator

class AWSSimulator(AWSAbstractSimulator):
    def __init__(self, *args, **kwargs):
        super(AWSSimulator, self).__init__(*args, **kwargs)

    @classmethod
    def mode_name(cls):
        return 'Cloud'

    # SIMULATE
    def simulate(self, experiment):
        super(AWSSimulator, self).simulate(experiment)
        instance_name = f"{experiment.at('name')} ({self.timestamp})"
        return self._AWSAbstractSimulator__run_instance(instance_name, self.output_path, experiment)