#!/bin/bash
echo "start" >> /userdata_configuration.log &&

apt update &&
echo "apt update" >> /userdata_configuration.log &&
apt upgrade -y &&
echo "apt upgrade" >> /userdata_configuration.log &&

mkdir -p /tmp/ssm &&
cd /tmp/ssm &&
wget https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/debian_amd64/amazon-ssm-agent.deb &&
echo "wget ssm.deb" >> /userdata_configuration.log &&
snap remove amazon-ssm-agent &&
dpkg -i amazon-ssm-agent.deb &&
systemctl enable amazon-ssm-agent &&
echo "ssm installation" >> /userdata_configuration.log &&

apt install -y git &&
su ubuntu -c "git clone https://github.com/aws/efs-utils ~/efs-utils" &&
echo "git clone efs-utils" >> /userdata_configuration.log &&
apt install make &&
apt install binutils &&
su ubuntu -c "cd ~/efs-utils" &&

./build-deb.sh
echo "build efs-utils" >> /userdata_configuration.log &&
apt install -y ./build/amazon-efs-utils.deb
echo "efs-utils installation" >> /userdata_configuration.log &&
apt install -y nfs-common &&
echo "nfs-common installation" >> /userdata_configuration.log &&

file_system_id_1={efs_volume_id} &&
efs_mount_point_1={efs_mount_path} &&
mkdir -p {efs_mount_path} &&
test -f "/sbin/mount.efs" && printf "\n{efs_volume_id}:/ {efs_mount_path} efs tls,_netdev\n" >> /etc/fstab || printf "\n{efs_volume_id}.efs.us-east-1.amazonaws.com:/ {efs_mount_path} nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport,_netdev 0 0\n" >> /etc/fstab &&
test -f "/sbin/mount.efs" && printf "\n[client-info]\nsource=liw\n" >> /etc/amazon/efs/efs-utils.conf &&
retryCnt=15; waitTime=30; while true; do mount -a -t efs,nfs4 defaults; if [ $? = 0 ] || [ $retryCnt -lt 1 ]; then echo File system mounted successfully; break; fi; echo File system not available, retrying to mount.; ((retryCnt--)); sleep $waitTime; done;
chown {user_id}:{group_id} {efs_mount_path} &&
echo "efs volume mounted" >> /userdata_configuration.log &&

echo "updating gama model environment" >> /userdata_configuration.log &&
cd {project_path} &&
git stash save &&
git stash drop &&
git pull &&
echo "gama model environment updated" >> /userdata_configuration.log &&

echo "command to simulate: su ubuntu -c 'cd {java_jdk_path}; ../../headless/gama-headless.sh {headless_file_path} {full_output_path}'" >> /userdata_configuration.log &&
su ubuntu -c "cd {java_jdk_path}; ../../headless/gama-headless.sh {headless_file_path} {full_output_path}" &&
echo "end" >> /userdata_configuration.log