is_concrete = lambda subclass: len(subclass.__subclasses__()) == 0

def all_concrete_subclasses(a_class):
	all_subclasses = []
	concrete_subclasses(a_class, all_subclasses)
	return all_subclasses

def concrete_subclasses(a_class, a_subclass_collection):
	for subclass in a_class.__subclasses__():
		a_subclass_collection.append(subclass) if is_concrete(subclass) else concrete_subclasses(subclass, a_subclass_collection)

class SuitableClassFinder():
	def __init__(self, abstract_class):
		self.abstract_class = abstract_class
		super().__init__()

	def suitable_for(self, *suitable_object, default_subclass=None, suitable_method='can_handle'):
		all_subclasses = all_concrete_subclasses(self.abstract_class)
		filtered_subclasses = [subclass for subclass in all_subclasses if (getattr(subclass, suitable_method)(*suitable_object))]
		if (len(filtered_subclasses) == 0) and (default_subclass is not None):
			return default_subclass
		elif (len(filtered_subclasses) == 1):
			return filtered_subclasses[0]
		elif (len(filtered_subclasses) > 1):
			raise ValueError("Many subclasses can handle the suitable object '{}'".format(suitable_object))
		else:
			raise ValueError("No subclass can handle the suitable object '{}'".format(suitable_object))