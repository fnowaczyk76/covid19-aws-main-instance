import os
import unittest
from src.headless.model_parameter import ModelParameter
from src.headless.model_parameters import ModelParameters

class TestModelParameters(unittest.TestCase):
    def test_creation(self):
        parameters = \
            ModelParameters().\
                set_experiment_parameters(
                    individual_amount=60000,
                    final_step=500,
                    display_size=50,
                    individual_speed=1,
                    starting_date='2020-01-01 00:00:00',
                    time_step_in_minutes=30,
                    steps_between_collect=6,
                    steps_between_dump=6,
                    seed=10).\
                set_epidemiological_parameters(
                    contact_distance_in_meters=1.5,
                    patient_zero_amount=1,
                    transmission_ratio=0.15,
                    asymptomatic_ratio=0.5,
                    mild_ratio=0.70,
                    severe_ratio=0.20,
                    icu_ratio=0.10,
                    mean_latent_days=6,
                    st_deviation_latent_days=1,
                    mean_incubation_days=8,
                    st_deviation_incubation_days=2,
                    mean_mild_recovery_days=12,
                    st_deviation_mild_recovery_days=3,
                    mean_severe_recovery_days=29,
                    st_deviation_severe_recovery_days=4,
                    mean_icu_recovery_days=30,
                    st_deviation_icu_recovery_days=4,
                    days_on_surfaces=3).\
                set_activity_parameters(
                    external_workers_amount=5000,
                    essential_workers_amount=8000,
                    supplying_ratio=0.01).\
                set_restriction_parameters(
                    restrictions_compliance_ratio=0.70,
                    people_get_self_quarantine=False,
                    quarantine_compliance_ratio=0.70,
                    allow_excepted=False,
                    schools_are_open=True,
                    travel_radius_in_km=5000).\
                set_coordinate_system(
                    coordinate_system_input='EPSG-3857',
                    coordinate_system_output='EPSG-4326').\
                set_resources_path(os.path.join(os.sep,'home','user','project'))

        self.assertEqual(parameters.at('individual_amount'), 60000)
        self.assertEqual(parameters.at('final_step'), 500)
        self.assertEqual(parameters.at('display_size'), 50)
        self.assertEqual(parameters.at('individual_speed'), 1)
        self.assertEqual(parameters.at('starting_date'), '2020-01-01 00:00:00')
        self.assertEqual(parameters.at('step'), 30)
        self.assertEqual(parameters.at('steps_between_collect'), 6)
        self.assertEqual(parameters.at('steps_between_dump'), 6)
        self.assertEqual(parameters.at('seed'), 10)
        self.assertEqual(parameters.at('contact_distance'), 1.5)
        self.assertEqual(parameters.at('patient_zero_amount'), 1)
        self.assertEqual(parameters.at('transmission_ratio'), 0.15)
        self.assertEqual(parameters.at('asymptomatic_ratio'), 0.5)
        self.assertEqual(parameters.at('mild_ratio'), 0.70)
        self.assertEqual(parameters.at('severe_ratio'), 0.20)
        self.assertEqual(parameters.at('icu_ratio'), 0.10)
        self.assertEqual(parameters.at('mean_latent_days'), 6)
        self.assertEqual(parameters.at('st_deviation_latent_days'), 1)
        self.assertEqual(parameters.at('mean_incubation_days'), 8)
        self.assertEqual(parameters.at('st_deviation_incubation_days'), 2)
        self.assertEqual(parameters.at('mean_mild_recovery_days'), 12)
        self.assertEqual(parameters.at('st_deviation_mild_recovery_days'), 3)
        self.assertEqual(parameters.at('mean_severe_recovery_days'), 29)
        self.assertEqual(parameters.at('st_deviation_severe_recovery_days'), 4)
        self.assertEqual(parameters.at('mean_icu_recovery_days'), 30)
        self.assertEqual(parameters.at('st_deviation_icu_recovery_days'), 4)
        self.assertEqual(parameters.at('days_on_surfaces'), 3)
        self.assertEqual(parameters.at('external_workers_amount'), 5000)
        self.assertEqual(parameters.at('essential_workers_amount'), 8000)
        self.assertEqual(parameters.at('supplying_ratio'), 0.01)
        self.assertEqual(parameters.at('restrictions_compliance_ratio'), 0.70)
        self.assertEqual(parameters.at('people_get_self_quarantine'), False)
        self.assertEqual(parameters.at('self_quarantine_compliance_ratio'), 0.70)
        self.assertEqual(parameters.at('allow_excepted'), False)
        self.assertEqual(parameters.at('schools_are_open'), True)
        self.assertEqual(parameters.at('travel_radius'), 5000)
        self.assertEqual(parameters.at('coordinate_system_input'), 'EPSG-3857')
        self.assertEqual(parameters.at('coordinate_system_output'), 'EPSG-4326')
        self.assertEqual(parameters.at('satellital_image_file_path'), os.path.join(os.sep,'home','user','project','includes','satellital','satellital.tif'))
        self.assertEqual(parameters.at('streets_shapefile_file_path'), os.path.join(os.sep,'home','user','project','includes','streets','streets.shp'))
        self.assertEqual(parameters.at('routes_shapefile_file_path'), os.path.join(os.sep,'home','user','project','includes','routes','routes.shp'))
        self.assertEqual(parameters.at('houses_shapefile_file_path'), os.path.join(os.sep,'home','user','project','includes','houses','houses.shp'))
        self.assertEqual(parameters.at('blocks_shapefile_file_path'), os.path.join(os.sep,'home','user','project','includes','blocks','blocks.shp'))
        self.assertEqual(parameters.at('workplaces_shapefile_file_path'), os.path.join(os.sep,'home','user','project','includes','workplaces','workplaces.shp'))
        self.assertEqual(parameters.at('hospitals_shapefile_file_path'), os.path.join(os.sep,'home','user','project','includes','hospitals','hospitals.shp'))
        self.assertEqual(parameters.at('train_stations_shapefile_file_path'), os.path.join(os.sep,'home','user','project','includes','train_stations','train_stations.shp'))
        self.assertEqual(parameters.at('frontiers_shapefile_file_path'), os.path.join(os.sep,'home','user','project','includes','frontiers','frontiers.shp'))
        self.assertEqual(parameters.at('census_radios_shapefile_file_path'), os.path.join(os.sep,'home','user','project','includes','census_radio','census_radio.shp'))

    def test_values(self):
        for parameter in ModelParameters().values():
            self.assertIsInstance(parameter, ModelParameter)

    def test_add(self):
        parameters = ModelParameters()
        self.assertNotIn('Mock parameter', parameters._values.keys())
        parameters.add(ModelParameter.suitable_by_value({'name':'Mock parameter', 'value':'Mock'}))
        self.assertIn('Mock parameter', parameters._values.keys())

    def test_at(self):
        parameters = ModelParameters()
        self.assertEqual(parameters.at('travel_radius'), 10)

    def test_at_key_error(self):
        parameters = ModelParameters()
        self.assertNotIn('Mock parameter', parameters._values.keys())
        with self.assertRaises(KeyError) as context:
            parameters.at('Mock parameter')
        self.assertEqual("'Parameter Mock parameter is not defined in current Model Parameters.'", str(context.exception))

    def test_drop(self):
        parameters = ModelParameters()
        self.assertNotIn('Mock parameter', parameters._values.keys())
        parameters.add(ModelParameter.suitable_by_value({'name':'Mock parameter', 'value':'Mock'}))
        self.assertIn('Mock parameter', parameters._values.keys())
        parameters.drop('Mock parameter')
        self.assertNotIn('Mock parameter', parameters._values.keys())

    def test_set_simulation_mode(self):
        parameters = ModelParameters()
        self.assertNotIn('mode', parameters._values.keys())
        parameters.set_simulation_mode('Local')
        self.assertIn('mode', parameters._values.keys())
        self.assertEqual(parameters.at('mode'), 'Local')

    def test_set_extra_parameters_file(self):
        expected_path = os.path.join(os.sep,'home','user','project','extra','path')
        parameters = ModelParameters()
        self.assertNotIn('extra_parameters_json_file_path', parameters._values.keys())
        parameters.set_extra_parameters_file(expected_path)
        self.assertIn('extra_parameters_json_file_path', parameters._values.keys())
        self.assertEqual(parameters.at('extra_parameters_json_file_path'), expected_path)

    def test_set_output_path(self):
        expected_path = os.path.join(os.sep,'home','user','project','output','path')
        parameters = ModelParameters()
        self.assertNotIn('output_path', parameters._values.keys())
        parameters.set_output_path(expected_path)
        self.assertIn('output_path', parameters._values.keys())
        self.assertEqual(parameters.at('output_path'), expected_path)

    def test_set_xml_headless_file_path(self):
        expected_path = os.path.join(os.sep,'home','user','project','headless','file')
        parameters = ModelParameters()
        self.assertNotIn('xml_headless_file_path', parameters._values.keys())
        parameters.set_xml_headless_file_path(expected_path)
        self.assertIn('xml_headless_file_path', parameters._values.keys())
        self.assertEqual(parameters.at('xml_headless_file_path'), expected_path)

if __name__ == '__main__':
    unittest.main()