# COVID19 Microsimulator - AWS Main Instance

## Installation (on AWS)

#### 1. Start an AWS instance (4 cores or above, 10 GB or more).

**Step 1:** Choose an appropiate Amazon Machine Image (AMI)
- e.g. Ubuntu Server 20.04 LTS (HVM), SSD Volume Type

![Step 1](/docs/installation/img/step1.png "Step 1")

**Step 2:** Choose an Instance Type
- e.g. c5n.xlarge

![Step 2](/docs/installation/img/step2.png "Step 2")

**Step 3:** Configure Instance Details
- All default, except for File Systems option:

![Step 3](/docs/installation/img/step3.png "Step 3")

**Step 4 and 5:**
- Configure it as your needs (or let the default values)

**Step 6:** Configure Security Group (SG)
- Select (or create) a SG with this inbound rules (set 2049 port for outbound too)

![Step 6](/docs/installation/img/step6.png "Step 6")

**Step 7:** Review Instance Launch
- Select (or create) a Key Pair (KP) for SSH connections (keep it in a safe place) and launch the main instance.

#### 2. Connect using SSH and the right KP.

`ssh -i "my-keypair.pem" ubuntu@the-public-instance-dns.compute-1.amazonaws.com`

#### 3. Verify Elastic File System (EFS) volume is mounted at `/mnt/efs/fs1`

`df -h`

![Verify EFS](/docs/installation/img/verify_efs.png "Verify EFS")

#### 4. Clone this repository on it:

`git clone https://gitlab.com/covid19-projects/covid19-microsimulation/covid19-aws-main-instance covid19-aws-main-instance`

#### 5. Enter to the project:

`cd covid19-aws-main-instance`

#### 6. set the right permissions to setup:

`chmod 777 setup.sh`

#### 7. Install:

`./setup.sh`

> ## [Back](/README.md)